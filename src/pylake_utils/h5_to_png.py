import datetime
from pathlib import Path
from typing import NamedTuple

import matplotlib.pyplot as plt
from lumicks import pylake
import numpy as np
import cv2


DATE_STR = "%Y-%m-%d-%H-%M-%S"


class FileDetails(NamedTuple):
    base_name: str
    pixel_size: float
    max_force: float
    time: datetime.datetime
    frame: int


def png_filename(name: str, time: datetime.datetime, pixel_size: float, max_force: float, frame: int=0) -> str:
    """
    Create a png filename encoding the additional data required to process the file.
    Args:
        name: base name of the scan
        time: the time the scan was taken
        pixel_size: the pixel size of the scan in microns
        max_force: the maximum force measured in force1x during acquisition
        frame: the frame index

    Returns:
        The file name encoded to have all the above data.
    """
    return f'{name}~{pixel_size:.3}~{max_force:.3}~{time.strftime(DATE_STR)}~{frame}'


def parse_png_filename(name: str) -> FileDetails:
    """
    get file details encoded in the filename
    Args:
        name: the name of the png file to be parsed

    Returns:
        FileDetails with all the details encoded in the filename
    """
    if name.lower().endswith('.png'):
        name = Path(name).stem
    details = name.split('~')
    if len(details) != 5:
        raise ValueError(f'{name}: unexpected structure of png file name, expected exactly 5 "~" characters')
    return FileDetails(
        details[0], float(details[1]), float(details[2]),
        datetime.datetime.strptime(details[3], DATE_STR), int(details[4])
    )


def save_h5_as_png(h5_path: Path, png_path: Path) -> None:
    """
    Go over all h5 files in the directory, and save a 16-bit per channel png for each frame\marker
    normalized so that its maximum value is 2^16 - 1 (the biggest number in 16 bit).
    Args:
        h5_path: The path with h5 files
        png_path: The path to save the png files to

    Returns:

    """
    if not png_path.exists():
        raise ValueError(f'png_path {png_path.absolute()} does not exist, create it first.')
    for f in h5_path.glob('*.h5'):
        h5 = pylake.File(str(f.absolute()))
        max_force = np.max(h5.force1x.data)
        time = datetime.datetime.fromtimestamp(h5.export_time // 1e9)
        multi_frame = False
        if len(h5.scans.values()) > 1:
            multi_frame = True
        for i, scan in enumerate(h5.scans.values()):
            if len(scan.green_image.shape) == 2:
                if multi_frame:
                    new_png_path = png_path / f.stem
                    new_png_path.mkdir(exist_ok=True)
                else:
                    new_png_path = png_path
                img = np.dstack([scan.red_image, scan.blue_image, scan.green_image])
                cv2.imwrite(
                    f'{new_png_path.absolute()}/{png_filename(f.stem, time, scan.pixelsize_um[0], max_force, i)}.png',
                    img.astype(np.uint16)
                )
            else:
                new_png_path = png_path / f'{f.stem}_{i}'
                new_png_path.mkdir(exist_ok=True)
                for j in range(scan.green_image.shape[0]):
                    img = np.dstack([scan.red_image[j, :, :], scan.blue_image[j, :, :], scan.green_image[j, :, :]])
                    cv2.imwrite(
                        f'{new_png_path.absolute()}/{png_filename(f.stem, time, scan.pixelsize_um[0], max_force, j)}.png',
                        img.astype(np.uint16)
                    )


def save_image_for_viewing(img: np.ndarray, pixel_size: float, title: str, png_path: Path) -> None:
    """
    Given an RGB  image, plot it for display (with a title and axes).
    Args:
        img: an RGB image
        pixel_size: the size of pixels in microns
        title: the title of the figure
        png_path: where to save the figure to

    Returns:

    """
    normalized = img / np.max(img)
    plt.imshow(normalized,
               extent=[0, img.shape[1] * pixel_size, 0, img.shape[0] * pixel_size])
    plt.title(title)
    plt.savefig(png_path)


def save_all_images_in_path_for_viewing(path: Path) -> None:
    """
    Create images for viewing all the scans in path (recursively), for all the images with the correct filename format
    Args:
        path: The path to search the pngs in

    Returns:

    """
    for f in path.rglob('*.png'):
        if f.stem.endswith('view'):
            continue
        img = cv2.imread(str(f.absolute()))
        new_path = f.parent / '../view'
        new_path.mkdir(exist_ok=True)
        try:
            details = parse_png_filename(f.name)
            save_image_for_viewing(
                img[:, :, ::-1], details.pixel_size,
                f"{details.base_name.replace('~', ' ')} {details.frame}", new_path / f'{f.stem}-view.png')
        except ValueError as ex:
            print(f'Skipped {f.absolute()} due to: {ex}')

