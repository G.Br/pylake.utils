## Read Me
This is a util lib for handling pylake files.
Currently, simplifies work with fluorescent scans.

#Usage:
First create pngs from h5 files with
save_h5_as_png

This will save the frames in the h5 file of the given path. 
If multiple paths exist in a single h5, it would create a subfolder named after that h5.

It's possible to create a plot of the image for viewing with axis and title using save_image_for_viewing

or save all the images under a certain path for viewing using save_all_images_in_path_for_viewing

Enjoy!
